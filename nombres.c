#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include "nombres.h"

//definitions des fonctions
listeChaine *creerListeChaine()
{
    //declarations des variables locales
    listeChaine *l=malloc(sizeof(listeChaine)),*p;
    int i,N,choix;
    float nb;
    //choix de l'initialisation de la liste
    printf("\nInitialiser la liste avec: \n1.Un seul nombre \n2.Plusieurs nombres"
           "\nEntrer 1 ou 2 pour continuer\n");
    scanf("%d",&choix);
    //inialisation de la liste par un seul nombre
    if(choix==1)
    {
        printf("\nEntrez le nombre a initialiser dans la liste\n");
        scanf("%f",&nb);
        l->nbr=nb;
        l->suiv=NULL;
        tete=l;
        return l;
    }
    //initialisation de la liste par plusieurs nombres
    else
    {
        tete=malloc(sizeof(listeChaine));
        printf("\nEntrez le nombre de nombres a initialiser dans la liste\n");
        scanf("%d",&N);
        p=tete;
        for(i=0; i<N; i++)
        {
            printf("\nEntrez le nombre %d: \n",i+1);
            scanf("%f",&p->nbr);
            p->suiv=malloc(sizeof(listeChaine));
            p=p->suiv;
        }
        p->suiv=NULL;
        supprimerNombreEnFinDeListeChaine(p);
        return p;
    }
}

listeChaine *ListeParDefaut()
{
    listeChaine *l=malloc(sizeof(listeChaine));
    l->nbr=0.00;
    l->suiv=NULL;
    tete=l;
    return l;
}

void afficherListeChaine(listeChaine *l)
{
    listeChaine *p=tete;
    printf("\nAffichage de la liste chainee:\n");
    while(p!=NULL)
    {
        printf("%.2f->",p->nbr);
        p=p->suiv;
    }
    printf("NULL\n");
}

int calculLongueurListeChaine(listeChaine *l)
{
    listeChaine*p=tete;
    int n=0;
    while(p!=NULL)
    {
        n=n+1;
        p=p->suiv;
    }
    return n;
}

void ajouterNombreEnTeteDeListeChaine(listeChaine *l, float val)
{
    listeChaine* nouveau=malloc(sizeof(listeChaine));
    if(nouveau==NULL)
        exit(0);
    nouveau->nbr=val;
    nouveau->suiv=tete;
    tete=nouveau;
}

void ajouterNombreEnFinDeListeChaine(listeChaine *l, float val)
{
    listeChaine* fin=malloc(sizeof(listeChaine)),*p;
    if(fin==NULL)
        exit(0);

    p=tete;
    while(p!=NULL)
    {
        if(p->suiv==NULL)
        {
            fin->nbr=val;
            p->suiv=fin;
            fin->suiv=NULL;
            break;
        }
        else
        {
            p=p->suiv;
        }
    }
}
void ajouterNombreEnPositionPDeListeChaine(listeChaine *l, float val, int position)
{
    int i;
    listeChaine *r,*p,*q;
    q= malloc(sizeof(listeChaine));
    if(position<0 || position>calculLongueurListeChaine(l)+1)
    {
        printf("\nCette position n'est pas valide dans la liste");
    }
    else if(position==1)
    {
        ajouterNombreEnTeteDeListeChaine(l,val);
        printf("\nNombre ajout� a la position %d avec succ�s!\n",position);
    }
    else if(position==calculLongueurListeChaine(l))
    {
        ajouterNombreEnFinDeListeChaine(l,val);
        printf("\nNombre ajout� a la position %d avec succ�s!\n",position);
    }
    else
    {
        p=tete;
        for(i=2; i<calculLongueurListeChaine(l); i++)
        {
            if(i==position)
            {
                q->nbr=val;
                r=p->suiv;
                p->suiv=q;
                q->suiv=r;
            }
            else
            {
                p=p->suiv;
            }
        }
        printf("\nNombre ajout� a la position %d avec succ�s!\n",position);
    }
}
void supprimerNombreEnTeteDeListeChaine(listeChaine *l)
{
    listeChaine* nouveau=malloc(sizeof(listeChaine));
    if(tete==NULL)
    {
        printf("\liste vide!");
    }
    else
    {
        nouveau=tete;
        tete=tete->suiv;
        free(nouveau);
        printf("\n Le nombre en t�te de la liste supprim� avec succ�s!\n");
    }
}
void supprimerNombreEnFinDeListeChaine(listeChaine *l)
{
    listeChaine * p=malloc(sizeof(listeChaine)),*t;
    p=tete;
    if(p==NULL)
    {
        printf("\nliste vide!");
    }
    else if(l==NULL)
    {
        printf("\nliste vide!");
    }
    else if(calculLongueurListeChaine(l)==1)
    {
        printf("\nliste est de longueur 1 donc tete de liste est fin de liste");
        supprimerNombreEnTeteDeListeChaine(l);
    }
    else
    {
        while(p!=NULL)
            if(p->suiv->suiv==NULL)
            {
                t=p->suiv;
                p->suiv=NULL;
                free(t);
                printf("\n Le nombre en fin de la liste supprim� avec succ�s!\n");
                break;
            }
            else
            {
                p=p->suiv;
            }
    }
}
void supprimerNombreEnPositionPDeListeChaine(listeChaine *l, int position)
{
    int i=1;
    listeChaine *p,*q;
    q= malloc(sizeof(listeChaine));
    if (l==NULL)
    {
        printf("liste vide!\n");
    }
    else if(position<0 || position>calculLongueurListeChaine(l)+1)
    {
        printf("\nCette position n'est pas valide dans la liste");
    }
    else
    {
        if(position==1)
        {
            supprimerNombreEnTeteDeListeChaine(l);
            printf("\nNombre � la position %d de la liste supprim� avec succ�s!\n",position);
        }
        else if(position==calculLongueurListeChaine(l))
        {
            supprimerNombreEnFinDeListeChaine(l);
            printf("\nNombre � la position %d de la liste supprim� avec succ�s!\n",position);
        }
        else
        {
            p=tete;
            while(p!=NULL && i<position)
            {
                i++;
                q=p;
                p=p->suiv;
            }
            if(p==NULL)
            {
                printf("\nPosition n'est pas valide dans la liste");
            }
            else
            {
                q->suiv=p->suiv;
                free(p);
                printf("\nNombre � la position %d de la liste supprim� avec succ�s!\n",position);
            }
        }
    }
}

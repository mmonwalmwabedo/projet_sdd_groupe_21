#include <stdio.h>
#include <stdlib.h>
#include <locale.h>
#include "nombres.h"

//programme principal
int main()
{
    //declarations de variables globales
    listeChaine *L;
    int N,choix,post;
    float nombre;
    setlocale(LC_CTYPE,"");

    //attribution de la liste par defaut
    L=ListeParDefaut();

    //Affichage du menu
    printf("\nPROGRAMME DE MANIPULATION DE LISTE CHAINEE DE NOMBRES\n");
    do
    {
        printf("\nVoici les op�rations diponibles dans ce programme: ");
        printf("\n 1.  Cr�er et Initialiser une liste chain�e"
               "\n 2.  Utiliser la liste d�j� cr��e par defaut: initialis�e avec 0"
               "\n 3.  Ajouter un nombre en t�te de la liste"
               "\n 4.  Ajouter un nombre en fin de la liste"
               "\n 5.  Ajouter un nombre en une position p de la liste"
               "\n 6.  Supprimer un nombre en t�te de la liste"
               "\n 7.  Supprimer un nombre en fin de la liste"
               "\n 8.  Supprimer un nombre en une position p de la liste"
               "\n 9.  Calculer la longueur de la liste"
               "\n 10. Afficher toute la liste chain�e"
               "\n 0. Pour fermer le programme, tapez 0\n");

        //Verification de la cr�ation d'une liste
        printf("\nSVP La cr�ation d'une liste avant toute autre op�ration;\n"
               " sinon <<liste par defaut>> sera attribu�e\n");

        //Choix de l'utilisateur et appel des fonctions
        printf("\nEntrez le num�ro de votre op�ration\n");
        scanf("%d",&choix);

        switch(choix)
        {
        case 1: //creation et initialiation de la liste
            L=creerListeChaine();
            printf("\nListe cr��e et initialis�e avec succ�s!\n");
            break;

        case 2: //utilisation de la liste par defaut
            L=ListeParDefaut();
            printf("\nListe par defaut choisie avec succ�s!\n");
            break;

        case 3: //Ajout d'un nombre en tete de la liste
            printf("\nEntrez le nombre � ajouter: ");
            scanf("%f",&nombre);
            ajouterNombreEnTeteDeListeChaine(L,nombre);
            printf("\nNombre ajout� en t�te de la liste avec succ�s!\n");
            break;

        case 4: //Ajout d'un nombre en fin de la liste
            printf("\nEntrez le nombre � ajouter: ");
            scanf("%f",&nombre);
            ajouterNombreEnFinDeListeChaine(L,nombre);
            printf("\nNombre ajout� en fin de la liste avec succ�s!\n");
            break;

        case 5: //Ajout d'un nombre en une position p de la liste
            printf("\nEntrez le nombre � ajouter: ");
            scanf("%f",&nombre);
            printf("\nEntrez la position d'ajout: ");
            scanf("%d",&post);
            ajouterNombreEnPositionPDeListeChaine(L,nombre,post);
            break;

        case 6: //Suppression d'un nombre en tete de la liste
            supprimerNombreEnTeteDeListeChaine(L);
            break;

        case 7: //Suppression d'un nombre en fin de la liste
            supprimerNombreEnFinDeListeChaine(L);
            break;

        case 8: //Suppression d'un nombre en une position p de la liste
            printf("\nEntrez la position � laquelle vous voulez supprimer le nombre\n");
            scanf("%d",&post);
            supprimerNombreEnPositionPDeListeChaine(L,post);
            break;

        case 9: //Calcul de la longueur de la liste
            N=calculLongueurListeChaine(L);
            printf("\nLa longueur de la liste chain�e est %d: \n",N);
            break;

        case 10: //affichage de la liste
            afficherListeChaine(L);
            printf("\n");
            break;

        case 0: //Fermeture du programme
            printf("\nProgramme ferm� avec succ�s!"
                   "\nTapez la touche enter pour quitter la page...\n");
            break;

        default: //Message par defaut pour une entr�e invalide lors du choix
            printf("\nEntrez un choix valide!");
        }
    }
    while(choix!=0);

    return 0;
}
